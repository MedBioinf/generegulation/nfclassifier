# Nucleosome Positioning

## Background
The positioning of nucleosomes is important for the regulation of transcription. This classifier uses a random forest to distinguish between nucleosomal and linker sequences. The training data comes from Guo et al. 2014. The process starts with a FASTA file, each containing sequences of the length 147 bp and internally transforms the data to a frequency spectrum (power spectral density) of derived DNAshape data (Minor Groove Width, Propeller Twist, Helix Twist, Roll). A genome-wide BIGWIG file containing 50bp-resolution predictions for the human genome, as well as a detailed version with 7bp-resolution and a continous score for all human promoters, can be found [here](https://bioinformatics.umg.eu/resources/gene-regulation-group-tool/).    

## Install
* Make sure you have the following R packages installed:
```
install.packages("getopt")
install.packages("randomForest")
install.packages("parallel")
install.packages("data.table")
BiocManager::install("DNAshapeR")
```

## Run
* Go to the `/code` directory
* Run the tool on your FASTA file
```
./predict -i your_fasta_file.fa -o prediction_output.tsv -c number_of_cores
```
* To run an example, execute the tool on the included dataset
```
./predict -i ../data/test_fasta.fa -o test_fasta_output.tsv
```

### Arguments

| Parameter |  |
| -------- | -------- |
| -i     | Input Fasta File. All your sequences to predict as nucleosomal/linker. The input needs to be 147bp in length and should not contain N nucleotides |
| -o     | Path to the output file. The output is a file with two TAB-separated columns. **First column**: The original IDs from the input FASTA file. **Second column**:  The prediction result (nucleosomal/linker)|
| -c     | Number of cores available for parallel processing. Default is 1. |

## Acknowledgements
The raw FASTA files for training the classifier are taken from:

Shou-Hui Guo, En-Ze Deng, Li-Qin Xu, Hui Ding, Hao Lin, Wei Chen, and Kuo-Chen Chou. iNuc-PseKNC: a sequence-based predictor for predicting nucleosome positioning in genomes with pseudo k-tuple ucleotide composition. *Bioinformatics*, 30(11):1522–1529, February 2014.

Their data is in turn derived from:

Dustin E. Schones, Kairong Cui, Suresh Cuddapah, Tae Young Roh, Artem Barski, Zhibin Wang, Gang Wei, and Keji Zhao. Dynamic regulation of nucleosome positioning in the human genome. *Cell*, 132(5):887–898, March 2008.
